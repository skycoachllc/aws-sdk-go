// Package aws contains support code for the various AWS clients in the
// bitbucket.org/synapse/aws-sdk-go/gen subpackages.
package aws
